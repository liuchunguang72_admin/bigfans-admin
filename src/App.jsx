import React, { Component } from 'react';
import { Layout, Menu, Icon ,Affix} from 'antd';
import SiderCustom from 'components/SiderCustom'

import {Link} from 'react-router-dom'

import StorageUtils from 'utils/StorageUtils' 
import AppHelper from 'utils/AppHelper'
import './App.css';

const { Header, Sider, Content } = Layout;

class App extends Component {

  constructor(props) {
    super(props)
    if(AppHelper.mode !== 'dev'){
      let token = StorageUtils.getToken()
      if(!token){
        this.props.history.push('/login')
      }
    }
  }

  state = {
    collapsed: false,
  };
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }



  render() {
    return (
      <div className="App">
        <Layout>
        <SiderCustom path={this.props} collapsed={this.state.collapsed}/>
        <Layout style={{ marginLeft: 200 }}>
          <Affix>
          <Header style={{ background: '#fff', padding: 0 }}>
          </Header>
          </Affix>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
      </div>
    );
  }
}

export default App;
