import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import {Link} from 'react-router-dom'
import HttpUtils from 'utils/HttpUtils'

class AttributeList extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount(){
		this.props.setSearchList(this)
	}

    columns = [
       { title: '属性名称', width: 200, dataIndex: 'name', key: 'name',fixed: 'left'},
	   { title: '商品分类', width: 100, dataIndex: 'catName', key: 'catName'},
	   { title: '是否为搜索项', dataIndex: 'searchableLabel', key: 'searchable', width: 130 },
	   { title: '输入方式', dataIndex: 'inputTypeLabel', key: 'inputType', width: 100 },
	   { title: '可选值', dataIndex: 'address', key: '4', width: 130 },
	   { title: '是否必选', dataIndex: 'requiredLabel', key: 'required', width: 130 },
	   { title: '排序', dataIndex: 'orderNum', key: 'orderNum', width: 100 },
	   {
	     title: 'Action',
	     key: 'operation',
	     fixed: 'right',
	     width: 100,
	     render: (text, record) => this.renderAction(text, record),
	   },
	];

	renderAction(text, record){
		return (
				<div key={record.id}>
					<Link to={"/attribute/" + record.id}>编辑</Link>
					<a onClick={(e) => this.deleteAttr(record)}>删除</a>
				</div>
			)
	}

	deleteAttr(record){
		let self = this;
		HttpUtils.deleteAttribute(record.id , {
			success(resp){
				self.props.onDeleteAttr(record)
			}
		})
	}

	render () {
		const rowSelection = {
		  onChange: (selectedRowKeys, selectedRows) => {
		    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
		    this.props.onSelectChange(selectedRowKeys, selectedRows);
		  },
		};
		return (
				<Table columns={this.columns} 
					   size="middle" 
					   bordered
					   rowSelection={this.props.showCheckbox && rowSelection}
					   pagination={this.props.pagination}
					   dataSource={this.props.dataSource} 
					   scroll={{ x: 1500, y: 300 }}
					   loading={this.props.loading}
					   onChange={this.handleTableChange} />
			)
	}
}

export default AttributeList;