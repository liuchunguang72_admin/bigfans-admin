import React from 'react';
import {Form, Input, Breadcrumb, Card, Cascader, Select, Row, Col, Checkbox, Button, message, Switch,Radio} from 'antd';
import AppHelper from 'utils/AppHelper'
import HttpUtils from 'utils/HttpUtils'
import CategoryUtils from 'utils/CategoryUtils'
import StringUtils from 'utils/StringUtils'

const FormItem = Form.Item;

/**
 * 规格编辑表单
 */
class EditForm extends React.Component {

    state = {
        inputType : 'M',
        categories : [],
        spec : {}
    }

    onCategoryChange = () => {
        console.info('category changed');
    }

    onInputTypeChange = (e) => {
        let spec = this.state.spec
        spec.inputType = e.target.value
        this.setState({spec})
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        //let name = this.props.form.getFieldValue('name');
        //let origin = this.props.form.getFieldValue('origin');
        // 获取表单中数据
        let formVals = this.props.form.getFieldsValue();
        let self = this;
        formVals.categoryId = formVals.categoryId.pop();
        message.loading('保存中',0);
        HttpUtils.updateSpec(formVals , this.props.specId ,{
            success(resp){
                message.destroy()
                self.props.history.push('/specs')
            }
        })
    }

    componentDidMount () {
        let self = this;
        HttpUtils.listCategories({
            success(resp){
                let categories = AppHelper.tools.formatCategories(resp.data , true);
                self.setState({categories});
            }
        });

        this.fetch()
    }

    fetch(){
        let self = this;
        HttpUtils.getSpec(this.props.specId , {
            success(resp) {
                self.setState({spec : resp.data})
            }
        })
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        const catIds = CategoryUtils.populateParents(this.state.categories , this.state.spec.categoryId)
        const optionValues = StringUtils.split(this.state.spec.values , ',')

        return (
            <Form onSubmit={e => {this.handleSubmit(e)}}>
                <FormItem {...formItemLayout} label="商品类别" >
                    {getFieldDecorator('categoryId', {
                        initialValue: catIds,
                        rules: [{
                            type: 'array',
                            required: true,
                            message: '请选择商品类别!'
                        }],
                    })(
                        <Cascader options={this.state.categories} onChange={this.onCategoryChange} changeOnSelect/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="规格名称" >
                    {getFieldDecorator('name', {
                        initialValue: this.state.spec.name,
                        rules: [{required: true, message: '请填写规格名称!', whitespace: true}],
                    })(
                        <Input/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="排序">
                    {getFieldDecorator('orderNum', {
                        initialValue: this.state.spec.orderNum
                    })(
                        <Input placeholder="根据排序进行由小到大排列显示" type="number"/>
                    )}
                </FormItem>
                <FormItem {...formItemLayout} label="输入方式">
                    {getFieldDecorator('inputType' , {
                        initialValue: this.state.spec.inputType,
                    })(
                        <Radio.Group onChange={this.onInputTypeChange}>
                            <Radio value='L'>从下面值中选择</Radio>
                            <Radio value='M'>手动输入</Radio>
                        </Radio.Group>
                    )}
                </FormItem>
                {
                    this.state.spec.inputType == 'L' ? 
                    <FormItem {...formItemLayout} label="可选值" hasFeedback>
                        {getFieldDecorator('valueList' , {
                            initialValue: optionValues,
                        })(
                            <Select
                                mode="tags"
                                style={{ width: '100%' }}
                                searchPlaceholder="标签模式"
                                placeholder="输入后按回车确认"
                            >
                            </Select>
                        )}
                    </FormItem>
                    :
                    ''
                }
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" size="large">更新规格</Button>
                </FormItem>
            </Form>
        );
    }

}
const SpecEditForm = Form.create()(EditForm);
export default SpecEditForm;