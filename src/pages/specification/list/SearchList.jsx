import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import {Link} from 'react-router-dom'
import HttpUtils from 'utils/HttpUtils'

class SpecList extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount(){
		this.props.setSearchList(this)
	}

	columns = [
	  	{ title: '规格名', width: 150, dataIndex: 'name', key: 'name' ,fixed: 'left'},
	  	{ title: '商品分类', width: 150, dataIndex: 'catName', key: 'catName'},
	  	{ title: '输入方式', dataIndex: 'inputTypeLabel', key: 'inputType', width: 100 },
	  	{ title: '可选项', dataIndex: 'values', key: 'values', width: 150 },
	  	{ title: '排序', dataIndex: 'orderNum', key: 'orderNum', width: 100 },
	  	{
		    title: 'Action',
		    key: 'operation',
		    fixed: 'right',
		    width: 100,
		    render: (text, record) => this.renderAction(text, record),
	  	},
	]

	renderAction(text, record){
		return (
				<div key={record.id}>
					<Link to={"/spec/" + record.id}>编辑</Link>
					<a onClick={(e) => this.deleteSpec(record)}>删除</a>
				</div>
			)
	}

	deleteSpec(record){
		let self = this;
		HttpUtils.deleteSpec(record.id , {
			success(resp){
				self.props.onDeleteSpec(record)
			}
		})
	}

	render () {
		const rowSelection = {
		  onChange: (selectedRowKeys, selectedRows) => {
		    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
		    this.props.onSelectChange(selectedRowKeys, selectedRows);
		  },
		};
		return (
				<Table columns={this.columns} 
					   size="middle" 
					   bordered
					   rowSelection={this.props.showCheckbox && rowSelection}
					   pagination={this.props.pagination}
					   dataSource={this.props.dataSource} 
					   scroll={{ x: 1500, y: 500 }}
					   loading={this.props.loading}
					   onChange={this.handleTableChange} />
			)
	}
}

export default SpecList;